# THIS HELPER SCRIPT NEEDS TO WORK WITH ZSH, AND BASH
# posix sh is not good enough due to use of arrays

# Which gcc releases we know about/support. Each version listed here as a key
# has a corresponding gcc-$ver-source package in Debian
declare -A known_debian_gcc_releases # maps from release version to debian version

# These lines are modified by auto_build_all.sh. Do NOT change the structure of
# these so that the script can keep working
known_debian_gcc_releases[4.9]="4.9.4-2"
known_debian_gcc_releases[5]="5.5.0-12"
known_debian_gcc_releases[6]="6.5.0-2"
known_debian_gcc_releases[7]="7.5.0-5"
known_debian_gcc_releases[8]="8.3.0-28"
known_debian_gcc_releases[9]="9.2.1-28"
known_debian_gcc_releases[10]="10.2.1-1"
known_debian_gcc_releases[11]="11.2.0-12"
known_debian_gcc_releases[12]="12-20211126-1"
known_debian_gcc_releases[13]="13-20230114-1"


# bash and zsh get associative array keys differently
declare -a known_gcc_releases
if [ -n "$ZSH_VERSION" ]; then
    known_gcc_releases=${(k)known_debian_gcc_releases[@]}
else
    known_gcc_releases=${!known_debian_gcc_releases[@]}
fi


# takes a single commandline argument: gcc release version. If we know about
# this release, we exit successfully
function validate_gcc_source_release
{
    local query_ver=$1

    if [ -z "$query_ver" ]; then
        echo "validate_gcc_source_release needs an argument" > /dev/stderr
        return 1;
    fi


    if [ -n "${known_debian_gcc_releases[$query_ver]}" ]; then
        return 0;
    else
        return 1;
    fi
}

function get_debian_release
{
    local gcc_release=$1

    if [ -z "$gcc_release" ]; then
        echo "get_debian_release needs an argument" > /dev/stderr
        return 1;
    fi

    local debian_release=${known_debian_gcc_releases[$gcc_release]}

    if [ -z "debian_release" ]; then
        echo "Unknown debian release for gcc release '$gcc_release'" > /dev/stderr
        return 1;
    fi

    echo "$debian_release";
    return 0;
}
